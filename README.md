# a Statamic Contact Form Plugin by [Robin Metcalfe](http://solarisedesign.co.uk)
## A simple contact form plugin for the Statamic CMS

This is a contact form plugin for Statamic. Designed to provide basic contact form functionality, and cover a few issues that I commonly encounter with contact forms and to provide functionality for my own website. I've made the source of the plugin available under the MIT license in the hope it may assist others. (feel free to use this, but as the license states, I take no responsibility for any mishaps that may occur along the way)

Features:

 * Detection for multiple submission attempts
 * User notification via email on success
 * HTML5 validation error checking
 * ...along with server-side error checking
 * Flexibility for form layout, class names etc.
 * Google reCAPTCHA integration
 * Integration with Statamic's mail capabilities (use services such as Mailgun etc.)

### How do I install this?

Simply copy the file pi.solarise_contact.php into the folder solarise_contact within the _add-ons directory of your Statamic installation

### Dependencies

This is because there are some features of form input/validation such as in-line error display and placeholder text that aren't supported by 
older browsers but which do have reasonably elegant Javascript based solutions.

If you wish to make use of the JS solutions, then also include the following file in your page (or merge/minify/concat with your own script, it matters not)

    /_add-ons/solarise_contact/script.js

### How do I configure this?

Copy the contents of the file solarise_contact.yaml.sample into the file solarise_contact.yaml located within the _config/addons/solarise_contact folder

Instructions for configuring the form are included as comments within the configuration file

You can also override any of the configuration parameters within the tag-pair itself, e.g.

    {{ solarise_contact button_label='Something else' }}
    ...

The individual field entries are defined within the 'fields' array. Four input types are currently supported, 'text', 'textarea', 'select' and 'email'

Inputs can also have the following configuration options set on a per-field button_class='wi

 * required (true/false)
 * title (the title to display within the form and on confirmation emails)
 * placeholder (shows a 'prompt' within the input, not applicable for select boxes)
 * options (for select inputs only. Define a range of inputs. At the moment, only allows values - no key/value pairs)

#### Username and Email are required fields

Some philosophical musings: I've purposefully made the username and email fields mandatory, albeit configurable (you can override title, label and placeholder)

My reasonings behind this are that this is a contact form plugin, and not a form plugin. To allow for arbitrary fields, I feel, would take away the "contact form"-ness of the plugin.

If you want a more powerful form solution for Statamic, I'd recommend checking out [Raven](http://statamic.com/add-ons/raven) from Statamic themselves. It's a much more powerful implementation of general forms and functionality.

(I created this plugin partly as an experiment and also because I needed a straightforward contact form solution. I could have easily used Raven, but I feel that 'contact form' is a general enough usage scenario that a simple little plugin like this was merited. Regardless, go show the [Statamic](http://statamic.com/) guys and gals some love by purchashing some licenses for stuff, or just send them good vibes. It's a wonderful little application)

### Javascript solutions for browser ineptitude

There will inevitably be people using the contact form who have the *utter cheek* to be visiting your site with an outdated browser.

As long as these people are still kind enough to have Javascript enabled, you can implement solutions that will provide fixes for the following:

 * Placeholder text doesn't show up in older browsers
   * Some older browsers such as e.g. --- and --- don't have inbuilt support for displaying placeholder content.
 * inline error display for e.g. missing a required field (showing an error without having to submit the form first)

I've deliberately chosen not to build JS support for these directly into my plugin, as

 - You may have your own preferred solution for implementing fixes for the above, and
 - The plugins listed do a good enough job of the task already, and I'm not one for reinventing the wheel (much)

If, of course, they are using an old browser *and* have Javascript disabled, then there's really no helping these poor souls, and they'll just have to make do without inline validation or placeholder content.

    <noscript>Turn Javascript back on. What is this, 2005?</noscript>

    <!--[if IE 6]>
    Congratulations! You are one of the last 12 people on the planet to be using IE6. I'm surprised this text even renders properly.
    <![endif]-->

### How do I set up the template?

The following is provided as a sample template outline. From this you can see

 * Any errors produced by the form itself are displayed clearly at the top
 * All input fields are available within a tag pair allowing for easy access to multiple input parameters
 * {{ before }} and {{ after }} contain the relevant <form> and </form> input tags along with a few other things

#### Code sample

    {{ solarise_contact button_class='wi-btn' button_label='Submit This' }}
        {{ before }}
        {{ config_errors }}
            <div class="config error">{{ value }}</div>
        {{ /config_errors }}
        {{ form_errors }}
            <div class="form error">{{ value }}</div>
        {{ /form_errors }}
        {{ inputs }}
            <div class='input {{ type }}'>
                {{ if errors }}
                    {{ errors }}
                        <div class="error">{{ value }}</div>
                    {{ /errors }}
                {{ /if }}
                <div class="input {{ type }}">
                    {{ label }}
                    {{ field }}
                </div>
            </div>
        {{ /inputs }}
        {{ after }}
    {{ /solarise_contact }}

For ease of use, especially if you plan on including the contact form in multiple places, copy this template code into a file within your partials folder (e.g. partials/contact-form.html), and then include where necessary throughout the site.

    {{ theme:partial src="contact-form" }}

You can further configure each instance of the form using parameters added to the tag-pair. If you're feeling sprightly, you could pass parameters *into the partial* which are then included in the tag-pair. I'll leave that as an exercise for the reader (because it's early and I haven't had breakfast yet and I don't want to type any more...)

### Error messages

Currently, the code is set up so that any serious error messages are displayed, as long as your Statamic installation is within a non "live" environment (see the 'environment' setting within settings.yaml) - In future, I may look at ways to easily specify in which enviornments errors should be reported, and in which they shouldn't

This simply means that during development, you can clearly see if anything goes wrong with the plugin, while showing no errors on a live site (it will simply return a blank space where the form should be)

### Tested on

I've tested this on

 * Chrome (latest build as of 13/02/15)
 * Firefox (latest build as of 13/02/15)
 * IE9
 * iOS Safari

If you notice any quirks or bugs, submit a report at @@@

### Support, Feature Requests, and Donations

For support, the best way to get in touch is via twitter: [@solarise_webdev](https://twitter.com/solarise_webdev).

If you found this plugin useful, please consider [donating] to show your appreciation

### Future development

At some point in the future, I plan to add

 * AJAX form submission (this will allow the plugin to work with HTML Caching)
 * More specific validation rules (e.g. regex matching, input lengths)
 * Entry logging via administration
 * Support for date and file upload fields
 * Newsletter signup checkbox (Mailchimp & Campaign Monitor)

### MIT License

Copyright (c) 2015 Robin Metcalfe

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.