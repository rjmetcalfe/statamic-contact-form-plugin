<?php

global $__js_loaded_pi_solarise_contact;

class Plugin_solarise_contact extends Plugin {

	// meta data for plugin
	public $meta = array(
	    'name'       => 'Solarise Contact Form',
	    'version'    => '1.0.1',
	    'author'     => 'Robin Metcalfe',
	    'author_url' => 'http://solarisedesign.co.uk'
	);

	// required settings in either the yaml config or the params within the tag
	private $default_settings = [
		'from_email' => null,
		'from_name' => null,
		'no_reply' => null,
		'send_to' => null,
		'subject' => 'An enquiry has been recieved',
		'target_page' => false,
		// user notification
		'notify_user' => true,
		'notify_user_message' => 'Dear %s\n\nThank you for your enquiry.\n\nSomeone will be in touch soon',
		'notify_user_subject' => 'Thank you for your enquiry',
		'cc' => false,
		'bcc' => false,
		'on_success' => 'text',
		'success_text' => '<h3>Thanks for your enquiry</h3><p>Your enquiry has been recieved and we\'ll get back to you soon',
		'success_page' => 'contact/success',
		'catch_spam' => null,
		'required_symbol' => '*',
		'fields' => [],
		'button_label' => 'Submit',
		'button_class' => 'button',
		'recaptcha' => true,
		'recaptcha_site_key' => '',
		'recaptcha_secret_key' => '',
		'error_recaptcha' => 'Please confirm you are a human',
		// errors
		'error_resubmit' => 'You are trying to resubmit the same form data again',
		'error_required' => '%s is a required field',
		'error_invalid_email' => 'Please provide a valid email address'
	];

	// stores the settings as read in with fetch() from either tag params, or the yaml config
	private $settings = [];

	// stores all the html inputs
	private $inputs = [];

	//configuration errors in setup
	private $config_errors = [];

	//error statuses for the whole form
	private $form_errors = [];

	//error statuses for individual fields
	private $field_errors = [];

	// store the results of $_POST
	private $post_results = [];
	
	// store a token to prevent form being submitted twice
	private $once_token;

	// if an attempt is made to resubmit a form (e.g. refresh)
	// set this, allows the form renderer to erase all values
	private $resubmission;

    /**
     * Inject the responsive image javascript into <head>
     * @return string
     */
	public function js() {
		global $__js_loaded_pi_solarise_contact;
		if($__js_loaded_pi_solarise_contact !== true) {
	        $script = Config::getSiteURL().Config::getAddOnsPath().'/solarise_contact/js/script.js';
	        $js = <<<JS
<script>
var s = document.createElement("script");
s.type = "text/javascript";
s.src = "{$script}";
document.body.appendChild(s);
</script>
JS;
			if($this->settings['recaptcha']) {
				$js .= <<<JS
<script>
var s2 = document.createElement("script");
s2.type = "text/javascript";
s2.src = "https://www.google.com/recaptcha/api.js";
document.body.appendChild(s2);
</script>
JS;
			}
	    } else {
	        $js = '';
	    }
	    $__js_loaded_pi_solarise_contact = true;
	    return $js;
	}

	/**
	 * The main tag-pair function
	 * @return type
	 */
	public function index() {
		// wrapped up in a nice little try/catch to more easily debug errors
		
		try {
			return $this->process();
		} catch(Exception $e) {
			if(Config::get('environment') != 'live') {				
				return $e->getMessage(); 
			} else {
				return false; // error, cannot display anything.
			}
		}
	}

	/**
	 * The main process function. Wrapped within a try/catch to display errors
	 * @return type
	 */
	private function process() {
		// check that:
		//  a) for each setting defined as required, there is either a value defined in config, or
		//  b) a default value exists for that param
		// otherwise, fail gracefully
		foreach($this->default_settings as $k => $default) {
			$value = $this->fetch($k, null, null, false, false);
			if($value !== null) {
				$this->settings[$k] = $value;
			} elseif($default !== null) {
				$this->settings[$k] = $default;
			} else {
				throw new Exception("You must specify a value for '{$k}'");
			}
		}


		// define default fields - name and email, mandatory. Everything else can be configured
		$mandatory = [
			'your_email' => [
				'title' => 'Your email address',
				'placeholder' => 'e.g. joe@bloggs.com',
				'type' => 'email',
				'required' => true,
				'label' => true
			],
			'your_name' => [
				'title' => 'Your name',
				'placeholder' => 'e.g. Joe Bloggs',
				'type' => 'text',
				'required' => true,
				'label' => true
			]
		];

		// pre-populate the field data with name and email
		foreach($mandatory as $field => $data) {
			if(isset($this->settings['fields'][$field])) {
				// already set some options? merge title and label settings only
				// todo: tidy this up a bit
				if(isset($this->settings['fields'][$field]['title'])) {
					$data['title'] = $this->settings['fields'][$field]['title'];
				}
				if(isset($this->settings['fields'][$field]['label'])) {
					$data['label'] = $this->settings['fields'][$field]['label'];
				}
				if(isset($this->settings['fields'][$field]['placeholder'])) {
					$data['placeholder'] = $this->settings['fields'][$field]['placeholder'];
				}
			}			
			$this->settings['fields'] = [$field => $data] + $this->settings['fields'];
		}

		try {
			// process() will check $_POST for the appropriate values
			// if successful, moves onto send_mails
			// and if that works too, then shows either a success message in place
			// of the form, or redirects to a new page
			if($this->process_post()) {
				if($this->send_mails()) {
					if($this->settings['on_success'] == 'page') {
						header('Location: '.$this->settings['success_page']);
						exit;
					} else {
						//1. either redirect to another page, or...
						//2. show a success message
						return "<a name='scj'></a>".$this->settings['success_text'];
					}
				}
			}
		} catch (Exception $e) {
			// while building the form inputs, if a configuration or other error
			// is encountered, fail gracefully			
			throw $e;
		}

		// just something a little bit random.
		// prevents the user from resubmitting the form data after a successful submission
		// (if detected, reloads the form and removes all values)
		$once_token = rand(1000000, 10000000);
		$this->session->set('once_token', $once_token);

		if($this->settings['target_page']) {
			$url = Config::getSiteUrl().'/'.$this->settings['target_page'];
		} else {
			$url = "";
		}

		$before = $this->js()."<a name='scj'></a><form action='{$url}#scj' method='POST'>\n<input type='hidden' name='once_token' value='{$once_token}' />\n";



		// process the field/input data
		try {
			$this->process_fields($this->settings['fields']);
		} catch(Exception $e) {
			//
			$this->config_errors[] = $e->getMessage().$e->getLine();
			throw $e;
		}

		// todo: move this out into a better class system to reuse different captcha systems...
		if($this->settings['recaptcha']) {
			$recaptcha = '<div class="g-recaptcha" data-sitekey="'.$this->settings['recaptcha_site_key'].'"></div>';
		} else {
			$recaptcha = false;
		}

		$after = "<button value='submit' class='{$this->settings['button_class']}' name='solarise_contact_submit' type='submit'>{$this->settings['button_label']}</button></form>";

		// the data to return to our statamic tag-pair
		return [
			'before' => $before,
			'config_errors' => $this->config_errors,
			'form_errors' => $this->form_errors,
			'inputs' => $this->inputs,
			'captcha' => $recaptcha,
			'after' => $after
		];
	}

	/**
	 * Process all fields and return formatted HTML input elements
	 * @param type $fields 
	 * @return type
	 */
	private function process_fields($fields) {
		foreach($fields as $field => $options) {
			if(!@$options['type'] || @!in_array($options['type'], ['text', 'textarea', 'select', 'email'])) {
				throw new Exception("A type (text, textarea, select or email) must be specified for {$field}");
			}
			//var_dump($options);
			$id = "solarise_contact_{$field}";
			$title = @isset($options['title'])?$options['title']:false;
			$input = [];
			if(isset($this->field_errors[$field])) {
				$input["errors"] = $this->field_errors[$field];
				$class = "error";
			} else {
				$class = "";				
				$input["errors"] = "";
			}

			//required attribute
			if(@$options['required']) {
				$required = "required='required'";
				$label_star = $this->settings['required_symbol'];;
			} else {
				$required = "";
				$label_star = '';
			}

			//placeholder attribute
			if(isset($options['placeholder'])) {
				$placeholder = $options['placeholder'];
			} else {
				$placeholder = "";
			}

			// get the value of this field via $_POST if present, AS LONG AS this is not a resubmission
			$value = isset($_POST[$field]) && !$this->resubmission ? $_POST[$field] : '';

			$input['type'] = $options['type'];

			if(isset($options['label']) && $options['label']) {
				$input['label'] = "<label for='{$id}'>{$title}{$label_star}</label>\n";
			} else {
				$input['label'] = false;
				//append "required" to placeholder
				$placeholder .= $label_star;
			}

			$placeholder = "placeholder='{$placeholder}'";

			switch($options['type']):
				case 'textarea':
					$input['field'] = "<textarea {$placeholder} {$required} class='{$class}' name='{$field}'>{$value}</textarea>";
					break;
				case 'select':
					$html = "<select {$required} class='{$class}' name='{$field}'>\n";
					if(isset($options['label']) && $options['label']) {
						$default = ' - Select - ';
					} else {
						$default = $options['title'].$label_star;
					}
					$html .= "<option value=''>{$default}</option>\n";
					foreach($options['options'] as $option) {
						//todo: indexed options?
						//todo: properly escape all inputs!!!
						if($value == $option) {
							$selected = 'selected="selected"';
						} else {
							$selected = '';
						}
						$html .= "<option {$selected} value='{$option}'>{$option}</option>\n";
					}
					$html .= "</select>\n";
					$input['field'] = $html;
					break;
				case 'text':
				case 'email':				
					$pattern = isset($options['pattern'])?"pattern='{$options['pattern']}'":'';
					$min = $max = $step = ''; //used for the number and range types
					//define validation patterns based on the input type
					if($options['type'] == 'email') {
						$pattern = '*@-.-';
					} elseif($options['type'] == 'url') {
						$pattern = 'https?://.+';
					} elseif($options['type'] == 'number') {

					} elseif($options['type'] == 'range') {

					}
					$input['field'] = "<input {$required} {$placeholder} class='{$class}' id='{$id}' type='{$options['type']}' value='{$value}' name='{$field}'  />\n";
					break;
				default:
					//throw new Exception("Unrecognised field type {$options['type']}");
					break;
			endswitch;
			$this->inputs[] = $input;
		}
		return true;
	}

	/**
	 * Process the data returned by $_POST
	 * At this stage, it only checks if a field is required or not
	 * Also verifies if an email address is valid, otherwise performs
	 * no additional complex checks
	 * @return type
	 */
	private function process_post() {
		if(isset($_POST['solarise_contact_submit'])) {
			// make sure a user can't submit a form twice by hitting refresh
			if($this->session->get('once_token') != $_POST['once_token']) {
				$this->form_errors[] = $this->settings['error_resubmit'];
				$this->resubmission = true;
				return false;
			}


			$error = false;

			// if google captcha is set, verify with Google
			if($this->settings['recaptcha']) {
				$response = $_POST['g-recaptcha-response'];
				$url = "https://www.google.com/recaptcha/api/siteverify?secret={$this->settings['recaptcha_secret_key']}&response={$response}";
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($curl, CURLOPT_TIMEOUT, 15);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE); 
				$curlData = curl_exec($curl);
				curl_close($curl);
		        $res = json_decode($curlData, TRUE);
		        if($res['success'] != 'true') {
		            $error = true;
		            $this->form_errors[] = $this->settings['error_recaptcha'];
		        }
			}


			// passed the once token check! delete the token now, its job is done
			$this->session->set('once_token', false);
			foreach($this->settings['fields'] as $field => $options) {
				$value = isset($_POST[$field]) ? trim($_POST[$field]) : false;
				if(isset($options['required']) && $options['required'] && ($value == '' || stripos($value, $options['placeholder']) !== false)) {
					if(!isset($this->form_errors[$field]) || !is_array($this->field_errors[$field])) {
						$this->field_errors[$field] = [];
					}
					$error = true;
					$this->field_errors[$field][] = sprintf($this->settings['error_required'], str_replace('_', ' ', $field));
				}

				// check validation params

				// check if emails are valid emails
				if($options['type'] == 'email') {
					//validate email
					if (!filter_var($_POST[$field], FILTER_VALIDATE_EMAIL)) {
					  $error = true;
					  $this->field_errors[$field][] = sprintf($this->settings['error_invalid_email'], $field);
					}
				}

				if(isset($_POST[$field])) {
					$this->post_results[$field] = [
						'title' => $options['title'],
						'value' => $_POST[$field]
					];
				}

			}
			return $error?false:true;
		} else {
			return false;
		}
	}

	/**
	 * Process and send emails using the configured Statamic email settings
	 * @return type
	 */
	private function send_mails() {
		
		$email = new Email;

		$mail_sent = false;

		// Construct text and html versions of the email
		$text = "A message was recieved\n\n";
		$html = "<h3>A message was recieved</h3>\n\n<table><thead><tr><td>Entry</td><td></td></tr></thead><tbody>";

		foreach($this->post_results as $field => $value) {
			$text .= "{$value['title']}: {$value['value']}\n\n";
			$html .= "<tr><td>{$value['title']}</td><td>{$value['value']}</td></tr>\n";
		}

		$html .= "</tbody></table>";

		// determine whether this email comes "from" the user, or the system
		if($this->settings['no_reply']) {
			$from = $this->settings['from_name'].' <'.$this->settings['from_email'].'>';
		} else {
			$from = $this->post_results['your_name']['value'].' <'.$this->post_results['your_email']['value'].'>';
		}

		$params = [
			'to' => $this->settings['send_to'],
			'from' => 'robin@solarisedesign.co.uk', //for testing, broken otherwise
			'text' => $text,
			'html' => $html,
			'subject' => sprintf($this->settings['subject'], $this->post_results['your_name']['value'])
		];

		if($this->settings['cc']) {
			$params['cc'] = is_array($this->settings['cc']) ? implode(',',$this->settings['cc']) : $this->settings['cc'];
		}

		if($this->settings['bcc']) {
			$params['bcc'] = is_array($this->settings['bcc']) ? implode(',',$this->settings['bcc']) : $this->settings['bcc'];
		}

		$email->send($params);

		// also send a thank-you to the user if specified
		if($this->settings['notify_user']) {
			$params = [
				'to' => $this->post_results['your_email']['value'],
				//'from' => $this->settings['from_name'].'<'.$this->settings['from_email'].'>',
				'from' => $this->settings['from_email'],
				'text' => sprintf($this->settings['notify_user_message'], $this->post_results['your_name']['value']),
				'html' => Parse::markdown(sprintf($this->settings['notify_user_message'], $this->post_results['your_name']['value'])),
				'subject' => $this->settings['notify_user_subject']
			];

			$email->send($params);
		}

		return true;
	}

}
