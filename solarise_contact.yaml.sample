# Set the from email address
from_email: noreply@example.com

# Set the name that this will show up as
from_name: Contact from

# Specify whether or not you should be able to hit "reply" to email the sender
# If 'yes', 'From' name/email will be as specified above
# If 'no', 'From' name/email will be the message sender's
no_reply: no

# The subject line of the email
# %1 = Submitter's full name
subject: A contact form enquiry from %s

# Specify who the form should send data to, as well as
# any cc or bcc settings. These can be either single values
# arrays, or csv values
send_to: youremail@example.com
cc:
  - email@example.com
  - email2@example.com
bcc:
  - email3@example.com

# Instead of having the form always submit data to the same page, specify an alternate page to which the data is submitted
# This is useful as it bypasses a problem with HTML caching, where a form wouldn't show changes as it would be permanently
# cached in a single state. By specifying an alternate page (and ensuring that the contact form is present on that page)
# This is useful if e.g. you plan on using a contact form in the footer of a page.
target_page: contact

on_success: page # either "page" or "text" - text replaces the contact form inline. page redirects the user to another defined page
success_text: Thank you for submitting your enquiry! Someone will be in touch soon.
success_page: contact/success

# Also thank the user with a custom email sent to their address (uses YAML multiline/NL preservation syntax)
notify_user: yes
notify_user_subject: Thanks for your enquiry
notify_user_message: |
  Dear %s

  Thanks for submitting your enquiry to the site.
  We'll review your message and get back to you soon

  Thanks,
  Admin

#whether to 'jump' the page down to the form on submit (uses URL location hash)
jump_to_form: true

# Enable the spam catcher service?
catch_spam: true

# The button text
button_label: "Submit enquiry"

# Customise all error messages
error_resubmit: You are trying to resubmit the same form data again
# %1 = Field title
error_required: %s is a required field

# The nuts and bolts. Specify your field list
# Currently supported field types are
# textarea (params limit)
# select (params options)
# text
fields:
  ##todo: make name and email default fields, add option for "first & last" or "full name"
  your_name:
    title: Your name
    placeholder: e.g. Joe Bloggs
    # currently supports: text, select (requires options), textarea, date, file
    type: text
    required: yes
    validate: #no validation for name, simply requires "not empty"
  your_email:
    title: Your email address
    placeholder: e.g. joe@bloggs.com
    type: email
    required: yes
    validate: #no validation options for email, automatic when type = email
  an_input:
    title: Another input box
    placeholder: Text input
    type: text
    required: yes
  enquiry_reason:
    title: The reason for your enquiry
    type: select
    options:
      - Technical enquiry
      - Sales enquiry
      - Feedback
    required: yes
  message:
    title: Your message
    type: textarea
    required: yes

